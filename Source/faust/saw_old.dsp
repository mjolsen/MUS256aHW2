// Music 256a / CS 476a | fall 2016
// CCRMA, Stanford University
//
// Author: Romain Michon (rmichonATccrmaDOTstanfordDOTedu)
// Description: Simple sawtooth synthesizer

import("stdfaust.lib");

// using the "old" ASDR because there is an issue with the new version
adsr2(a,d,s,r,t) = env ~ (_,_) : (!,_) // the 2 'state' signals are fed back
with 
{
    env (p2,y) =
        (t>0) & (p2|(y>=1)),          // p2 = decay-sustain phase
        (y + p1*u - (p2&(y>s))*v*y - p3*w*y)	// y  = envelop signal
	*((p3==0)|(y>=eps)) // cut off tails to prevent denormals
    with 
    {
        p1 = (p2==0) & (t>0) & (y<1);         // p1 = attack phase
	p3 = (t<=0) & (y>0);                  // p3 = release phase
	// #samples in attack, decay, release, must be >0
	na = ma.SR*a+(a==0.0); nd = ma.SR*d+(d==0.0); nr = ma.SR*r+(r==0.0);
	// correct zero sustain level
	z = s+(s==0.0)*ba.db2linear(-60);
	// attack, decay and (-60dB) release rates
	u = 1/na; v = 1-pow(z, 1/nd); w = 1-1/pow(z*ba.db2linear(60), 1/nr);
	// values below this threshold are considered zero in the release phase
	eps = ba.db2linear(-120);
    };
};

// smoo = smooth(0.999) which is just a "normalized one pole filter" configured as a lowpass
pureFreq = nentry("freq",440,20,20000,0.01);
freq = button("Portamento") <: //: si.smoo;
gain = nentry("gain",1,0,1,0.01) : si.smoo;
gate = button("gate") : vgroup("1-adsr",adsr2(attack,decay,sustain,release));
// ADSR parameters
attack  = hslider("attack", 0.01,0,1,0.001);
decay   = hslider("decay", 0.3,0,1,0.001);
sustain = hslider("sustain",0.5,0,1,0.01);
release = hslider("release",0.01,0,5,0.001);

process = hgroup("saw",os.sawtooth(freq) * gain * gate);

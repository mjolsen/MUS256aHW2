/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "faust/Saw.h"

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainContentComponent   : public AudioAppComponent, 
                               private Slider::Listener, 
                               private ComboBox::Listener,
                               private ToggleButton::Listener,
                               private MidiInputCallback,
                               private MidiKeyboardStateListener
{
public:
    //==============================================================================
    MainContentComponent() : currentSampleRate (0.0), lastInputIndex (0),
                             isAddingFromMidiInput (false),
                             keyboardComponent (keyboardState, MidiKeyboardComponent::horizontalKeyboard), 
                             startTime (Time::getMillisecondCounterHiRes() * 0.001), defGainVal(0.5), 
                             defAttackVal(0.1), defDecayVal(0.1), defSustainVal(0.5), defReleaseVal(1.0),
                             defLfoFreqVal(2.5)
    {
        setOpaque (true);
        labelsFont = Font ("Comfortaa", 16.0f, Font::bold);
        titleFont = Font ("Comfortaa", 35.0f, Font::bold);
        
        addAndMakeVisible (midiInputListLabel);
        midiInputListLabel.setText ("MIDI Input:", dontSendNotification);
        midiInputListLabel.attachToComponent (&midiInputList, true);

        addAndMakeVisible (midiInputList);
        midiInputList.setTextWhenNoChoicesAvailable ("No MIDI Inputs Enabled");
        const StringArray midiInputs (MidiInput::getDevices());
        midiInputList.addItemList (midiInputs, 1);
        midiInputList.addListener (this);

        // find the first enabled device and use that by default
        for (int i = 0; i < midiInputs.size(); ++i)
        {
            if (deviceManager.isMidiInputEnabled (midiInputs[i]))
            {
                setMidiInput (i);
                break;
            }
        }
        // if no enabled devices were found just use the first one in the list
        if (midiInputList.getSelectedId() == 0)
            setMidiInput (0);

        addAndMakeVisible (keyboardComponent);
        keyboardState.addListener (this);
        keyboardComponent.setKeyPressBaseOctave(3);
    
        // components to control Faust dsp
        addAndMakeVisible (gainSlider);
        gainSlider.setRange (0.0, 1.0);
        gainSlider.setValue(defGainVal);
        gainSlider.addListener (this);
        gainSlider.setSliderStyle (Slider::SliderStyle::Rotary);
        gainSlider.setTextBoxStyle (Slider::TextBoxBelow,true,
                                    gainSlider.getTextBoxWidth(),gainSlider.getTextBoxHeight());
        gainSlider.setColour (Slider::thumbColourId, Colour(0xaaede102));
        gainSlider.setColour (Slider::rotarySliderFillColourId, Colours::yellow);
        gainSlider.setColour (Slider::textBoxTextColourId, Colour (0xff01dd01));
        gainSlider.setColour (Slider::rotarySliderOutlineColourId, Colour (0x0f000000));
        gainSlider.setColour (Slider::textBoxBackgroundColourId, Colour (0x13000000));
        gainSlider.setColour (Slider::textBoxOutlineColourId, Colour (0x000000));
        
        addAndMakeVisible(gainLabel);
        gainLabel.setText ("Gain", dontSendNotification);
        gainLabel.setColour(0x1000281,Colour(0xaaede102));
        gainLabel.setFont(labelsFont);
        gainLabel.attachToComponent (&gainSlider, false);
        gainLabel.setJustificationType (Justification::centred);

        addAndMakeVisible (attackSlider);
        attackSlider.setRange (0.01, 1.0);
        attackSlider.setValue (defAttackVal);
        attackSlider.setTextValueSuffix (" s");
        attackSlider.addListener (this);
        attackSlider.setColour (Slider::thumbColourId, Colour (0xffada84c));
        attackSlider.setColour (Slider::trackColourId, Colour (0x2df8ff00));
        attackSlider.setColour (Slider::rotarySliderFillColourId, Colour (0xda0000ff));
        attackSlider.setColour (Slider::rotarySliderOutlineColourId, Colour (0xff000023));
        attackSlider.setColour (Slider::textBoxTextColourId, Colour (0xff01dd01));
        attackSlider.setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
        attackSlider.setColour (Slider::textBoxOutlineColourId, Colour (0x00808080));
        attackSlider.setSliderStyle (Slider::SliderStyle::LinearVertical);
        attackSlider.setTextBoxStyle (Slider::TextEntryBoxPosition::TextBoxBelow,true,
                                      attackSlider.getTextBoxWidth(),attackSlider.getTextBoxHeight());
        
        addAndMakeVisible (attackLabel);
        attackLabel.setText ("Attack", dontSendNotification);
        attackLabel.setColour(0x1000281,Colour(0xaaede102));
        attackLabel.setFont(labelsFont);
        attackLabel.attachToComponent (&attackSlider, false);
        attackLabel.setJustificationType (Justification::centred);

        addAndMakeVisible (decaySlider);
        decaySlider.setRange (0.01, 1.0);
        decaySlider.setValue (defDecayVal);
        decaySlider.setTextValueSuffix (" s");
        decaySlider.addListener (this);
        decaySlider.setColour (Slider::thumbColourId, Colour (0xffada84c));
        decaySlider.setColour (Slider::trackColourId, Colour (0x2df8ff00));
        decaySlider.setColour (Slider::rotarySliderFillColourId, Colour (0xda0000ff));
        decaySlider.setColour (Slider::rotarySliderOutlineColourId, Colour (0xff000023));
        decaySlider.setColour (Slider::textBoxTextColourId, Colour (0xff01dd01));
        decaySlider.setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
        decaySlider.setColour (Slider::textBoxOutlineColourId, Colour (0x00808080));
        decaySlider.setSliderStyle (Slider::SliderStyle::LinearVertical);
        decaySlider.setTextBoxStyle (Slider::TextEntryBoxPosition::TextBoxBelow,true,
                                     decaySlider.getTextBoxWidth(),decaySlider.getTextBoxHeight());
        
        addAndMakeVisible (decayLabel);
        decayLabel.setText ("Decay", dontSendNotification);
        decayLabel.setColour(0x1000281,Colour(0xaaede102));
        decayLabel.setFont(labelsFont);
        decayLabel.attachToComponent (&decaySlider, false);
        decayLabel.setJustificationType (Justification::centred);

        addAndMakeVisible (sustainSlider);
        sustainSlider.setRange (0.0, 1.0);
        sustainSlider.setValue (defSustainVal);
        sustainSlider.addListener (this);
        sustainSlider.setColour (Slider::thumbColourId, Colour (0xffada84c));
        sustainSlider.setColour (Slider::trackColourId, Colour (0x2df8ff00));
        sustainSlider.setColour (Slider::rotarySliderFillColourId, Colour (0xda0000ff));
        sustainSlider.setColour (Slider::rotarySliderOutlineColourId, Colour (0xff000023));
        sustainSlider.setColour (Slider::textBoxTextColourId, Colour (0xff01dd01));
        sustainSlider.setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
        sustainSlider.setColour (Slider::textBoxOutlineColourId, Colour (0x00808080));
        sustainSlider.setSliderStyle (Slider::SliderStyle::LinearVertical);
        sustainSlider.setTextBoxStyle (Slider::TextEntryBoxPosition::TextBoxBelow,true,
                                       sustainSlider.getTextBoxWidth(),sustainSlider.getTextBoxHeight());
        
        addAndMakeVisible (sustainLabel);
        sustainLabel.setText ("Sustain", dontSendNotification);
        sustainLabel.setColour(0x1000281,Colour(0xaaede102));
        sustainLabel.setFont(labelsFont);
        sustainLabel.attachToComponent (&sustainSlider, false);
        sustainLabel.setJustificationType (Justification::centred);

        addAndMakeVisible (releaseSlider);
        releaseSlider.setRange (0.01, 5.0);
        releaseSlider.setValue (defReleaseVal);
        releaseSlider.setTextValueSuffix (" s");
        releaseSlider.addListener (this);
        releaseSlider.setColour (Slider::thumbColourId, Colour (0xffada84c));
        releaseSlider.setColour (Slider::trackColourId, Colour (0x2df8ff00));
        releaseSlider.setColour (Slider::rotarySliderFillColourId, Colour (0xda0000ff));
        releaseSlider.setColour (Slider::rotarySliderOutlineColourId, Colour (0xff000023));
        releaseSlider.setColour (Slider::textBoxTextColourId, Colour (0xff01dd01));
        releaseSlider.setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
        releaseSlider.setColour (Slider::textBoxOutlineColourId, Colour (0x00808080));
        releaseSlider.setSliderStyle (Slider::SliderStyle::LinearVertical);
        releaseSlider.setTextBoxStyle (Slider::TextEntryBoxPosition::TextBoxBelow,true,
                                       releaseSlider.getTextBoxWidth(),releaseSlider.getTextBoxHeight());
        
        addAndMakeVisible (releaseLabel);
        releaseLabel.setText ("Release", dontSendNotification);
        releaseLabel.setColour(0x1000281,Colour(0xaaede102));
        releaseLabel.setFont(labelsFont);
        releaseLabel.attachToComponent (&releaseSlider, false);
        releaseLabel.setJustificationType (Justification::centred);

        addAndMakeVisible (lfoFreqSlider);
        lfoFreqSlider.setRange (0.001, 6.0);
        lfoFreqSlider.setValue (defLfoFreqVal);
        lfoFreqSlider.setTextValueSuffix (" Hz");
        lfoFreqSlider.addListener (this);
        lfoFreqSlider.setColour (Slider::thumbColourId, Colour (0xffada84c));
        lfoFreqSlider.setColour (Slider::trackColourId, Colour (0x2df8ff00));
        lfoFreqSlider.setColour (Slider::rotarySliderFillColourId, Colour (0xda0000ff));
        lfoFreqSlider.setColour (Slider::rotarySliderOutlineColourId, Colour (0xff000023));
        lfoFreqSlider.setColour (Slider::textBoxTextColourId, Colour (0xff01dd01));
        lfoFreqSlider.setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
        lfoFreqSlider.setColour (Slider::textBoxOutlineColourId, Colour (0x00808080));
        lfoFreqSlider.setSliderStyle (Slider::SliderStyle::LinearVertical);
        lfoFreqSlider.setTextBoxStyle (Slider::TextEntryBoxPosition::TextBoxBelow,true,
                                       releaseSlider.getTextBoxWidth(),releaseSlider.getTextBoxHeight());
        
        addAndMakeVisible (lfoFreqLabel);
        lfoFreqLabel.setText ("LFO Frequency", dontSendNotification);
        lfoFreqLabel.setColour(0x1000281,Colour(0xaaede102));
        lfoFreqLabel.setFont(labelsFont);
        lfoFreqLabel.attachToComponent (&lfoFreqSlider, false);
        lfoFreqLabel.setJustificationType (Justification::centred);

        addAndMakeVisible(bypassLfoButton);
        bypassLfoButton.addListener (this);
        bypassLfoButton.setButtonText (TRANS("Bypass LFO"));
        bypassLfoButton.setToggleState (true, dontSendNotification);
        bypassLfoButton.setColour (ToggleButton::textColourId, Colour(0xaaede102));
        
        addAndMakeVisible (titleLabel);
        titleLabel.setText("Space Bass", dontSendNotification);
        titleLabel.setColour(0x1000281,Colour(0xaaede102));
        titleLabel.setFont(titleFont);
        titleLabel.setJustificationType (Justification::left);

        addAndMakeVisible (autowahSlider);
        autowahSlider.setRange (0.001, 1.0);
        autowahSlider.setValue (0.5);
        autowahSlider.addListener (this);
        autowahSlider.setColour (Slider::thumbColourId, Colour (0xffada84c));
        autowahSlider.setColour (Slider::trackColourId, Colour (0x2df8ff00));
        autowahSlider.setColour (Slider::rotarySliderFillColourId, Colour (0xda0000ff));
        autowahSlider.setColour (Slider::rotarySliderOutlineColourId, Colour (0xff000023));
        autowahSlider.setColour (Slider::textBoxTextColourId, Colour (0xff01dd01));
        autowahSlider.setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
        autowahSlider.setColour (Slider::textBoxOutlineColourId, Colour (0x00808080));
        autowahSlider.setSliderStyle (Slider::SliderStyle::LinearVertical);
        autowahSlider.setTextBoxStyle (Slider::TextEntryBoxPosition::TextBoxBelow,true,
                                       releaseSlider.getTextBoxWidth(),releaseSlider.getTextBoxHeight());
        
        addAndMakeVisible (autowahLabel);
        autowahLabel.setText ("Autowah Amount", dontSendNotification);
        autowahLabel.setColour(0x1000281,Colour(0xaaede102));
        autowahLabel.setFont(labelsFont);
        autowahLabel.attachToComponent (&autowahSlider, false);
        autowahLabel.setJustificationType (Justification::centred);

        setSize (800, 350);
        nChans = 1;
        
        // specify the number of input and output channels that we want to open
        setAudioChannels (0, nChans);
        // allocate memory for audioBuffer object
        audioBuffer = new float*[nChans];
    }

    ~MainContentComponent()
    {
        shutdownAudio();
        
        // Faust stuff
        gainSlider.removeListener (this);
        attackSlider.removeListener (this);
        decaySlider.removeListener (this);
        sustainSlider.removeListener (this);
        releaseSlider.removeListener (this);
        lfoFreqSlider.removeListener (this);
        bypassLfoButton.removeListener (this);
        autowahSlider.removeListener (this);
        delete [] audioBuffer;
        
        // MIDI stuff
        keyboardState.removeListener (this);
        deviceManager.removeMidiInputCallback (MidiInput::getDevices()[midiInputList.getSelectedItemIndex()], this);
        midiInputList.removeListener (this);
    }

    //==============================================================================
    void prepareToPlay (int samplesPerBlockExpected, double sampleRate) override
    {
        // store the sample rate and block size
        currentSampleRate = sampleRate;

        saw.init(sampleRate); // initializing the Faust module
        saw.buildUserInterface(&sawControl); // linking the Faust module to the controller
        
        // Print the list of parameters address of "saw"
        // To get the current (default) value of these parameters, sawControl.getParamValue("paramPath") can be used
        for(int i = 0; i < sawControl.getParamsCount(); i++){
            std::cout << sawControl.getParamAdress(i) << "\n";
        }
        
        // setting default values for the Faust module parameters
        sawControl.setParamValue("/saw/freq",0);
        sawControl.setParamValue("/saw/gain",defGainVal);
        sawControl.setParamValue("/saw/bypasslfo",1.0f);
        sawControl.setParamValue("/saw/autowahamt",0.5f);
        sawControl.setParamValue("/saw/lfofreq",defLfoFreqVal);
        sawControl.setParamValue("/saw/1-adsr/attack",defAttackVal);
        sawControl.setParamValue("/saw/1-adsr/decay",defDecayVal);
        sawControl.setParamValue("/saw/1-adsr/sustain",defSustainVal);
        sawControl.setParamValue("/saw/1-adsr/release",defReleaseVal);
        
        // give keyboard focus, so we can play
        keyboardComponent.grabKeyboardFocus();
        keyboardComponent.setWantsKeyboardFocus(true);
        keyboardComponent.setLowestVisibleKey(20);
    }

    void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill) override
    {
        int blockSize = bufferToFill.numSamples;
        
        // compute the audio buffer values of the 1st channel
        audioBuffer[0] = bufferToFill.buffer->getWritePointer(0,bufferToFill.startSample);
        saw.compute(blockSize, NULL, audioBuffer);
        
        // if there are more channels
        if (bufferToFill.buffer->getNumChannels() > 1){
            // cycle through them and copy value of 1st channel to them
            for (int channel = 1; channel < bufferToFill.buffer->getNumChannels(); ++channel){
                // pointer to starting sample of this channel's buffer
                float* const buffer = bufferToFill.buffer->getWritePointer(channel,bufferToFill.startSample);
                // cycle through samples
                for (int sample = 0; sample < blockSize; ++sample)
                    // and copy value from 1st channel for current sample
                    buffer[sample] = bufferToFill.buffer->getSample(0,sample);
            }
        }
    }

    void releaseResources() override
    {
        // This will be called when the audio device stops, or when it is being
        // restarted due to a setting change.

        // For more details, see the help for AudioProcessor::releaseResources()
    }

    //==============================================================================
    void paint (Graphics& g) override
    {
        // You can add your drawing code here!
        Rectangle<int> areai (getLocalBounds());
        Rectangle<float> areaf ((float)areai.getX(),(float)areai.getY(),(float)areai.getWidth(),
                                (float)areai.getHeight());

        // image courtesy of: http://wallpapersafari.com/w/uYXW9J
        File f = (String)"~/Documents/Stanford/MUS256a/HW2/MUS256aHW2/Source/BlueSpace.jpg";
        Image img = ImageFileFormat::loadFrom(f);
        g.drawImageAt(img,0,0,false);
    }

    void resized() override
    {
        int div = 8, sHeight = 120, sY = 90;
        
        // rectangle object set to current size of main component
        Rectangle<int> area (getLocalBounds());
        // place all the controls on relevant areas of main component
        titleLabel.setBounds (16,8,getWidth()-32,40);
        midiInputList.setBounds (getWidth()/3,35,2*getWidth()/3-16,20);
        keyboardComponent.setBounds (area.removeFromBottom (126).reduced(8));
        gainSlider.setBounds (0, sY, (getWidth() - 10)/6, sHeight);
        attackSlider.setBounds ((getWidth() - 10)/div, sY, (getWidth() - 10)/div, sHeight);
        decaySlider.setBounds (2*(getWidth() - 10)/div, sY, (getWidth() - 10)/div, sHeight);
        sustainSlider.setBounds (3*(getWidth() - 10)/div, sY, (getWidth() - 10)/div, sHeight);
        releaseSlider.setBounds (4*(getWidth() - 10)/div, sY, (getWidth() - 10)/div, sHeight);
        lfoFreqSlider.setBounds (5*(getWidth() - 10)/div, sY, (getWidth() - 10)/div, sHeight);
        bypassLfoButton.setBounds (6*(getWidth() - 10)/div, sY, (getWidth() - 10)/div, sHeight);
        autowahSlider.setBounds (7*(getWidth() - 10)/div, sY, (getWidth() - 10)/div, sHeight);
    }

    void sliderValueChanged (Slider* slider) override
    {
        if (currentSampleRate > 0.0){
            // update Faust with changed slider values
            if (slider == &gainSlider){
                sawControl.setParamValue("/saw/gain", gainSlider.getValue());
            }
            else if (slider == &lfoFreqSlider){
                sawControl.setParamValue("/saw/lfofreq", lfoFreqSlider.getValue());
            }
            else if (slider == &autowahSlider){
                sawControl.setParamValue("/saw/autowahamt", autowahSlider.getValue());
            }
            else if (slider == &attackSlider){
                sawControl.setParamValue("/saw/1-adsr/attack", attackSlider.getValue());
            }
            else if (slider == &decaySlider){
                sawControl.setParamValue("/saw/1-adsr/decay", decaySlider.getValue());
            }
            else if (slider == &sustainSlider){
                sawControl.setParamValue("/saw/1-adsr/sustain", sustainSlider.getValue());
            }
            else if (slider == &releaseSlider){
                sawControl.setParamValue("/saw/1-adsr/release", releaseSlider.getValue());
            }
        }
    }

    void comboBoxChanged (ComboBox* box) override
    {
        if (box == &midiInputList)
            setMidiInput (midiInputList.getSelectedItemIndex());
    }

    void buttonClicked (Button* button) override
    {
        // turn portamento on or off
        if (button == &bypassLfoButton && bypassLfoButton.getToggleState()){
            sawControl.setParamValue("/saw/bypasslfo",1);
        }
        else{
            sawControl.setParamValue("/saw/bypasslfo",0);
        }
        keyboardComponent.grabKeyboardFocus();
    }

    /** Starts listening to a MIDI input device, enabling it if necessary. */
    void setMidiInput (int index)
    {
        const StringArray list (MidiInput::getDevices());

        deviceManager.removeMidiInputCallback (list[lastInputIndex], this);

        const String newInput (list[index]);

        if (! deviceManager.isMidiInputEnabled (newInput))
            deviceManager.setMidiInputEnabled (newInput, true);

        deviceManager.addMidiInputCallback (newInput, this);
        midiInputList.setSelectedId (index + 1, dontSendNotification);

        lastInputIndex = index;
    }
    
    // These methods handle callbacks from the midi device + on-screen keyboard..
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override
    {
        const ScopedValueSetter<bool> scopedInputFlag (isAddingFromMidiInput, true);
        keyboardState.processNextMidiEvent (message);
    }

    void handleNoteOn (MidiKeyboardState*, int midiChannel, int midiNoteNumber, float velocity) override
    {
        // set current time
        double curTime = (Time::getMillisecondCounterHiRes() * 0.001) - startTime;
        // set frequency in Hertz of midiNoteNumber
        double curFreq = MidiMessage::getMidiNoteInHertz(midiNoteNumber);
        
        // see if any noteOnStatuses is empty
        if (noteOnStatuses.empty()){
            // create noteOnStatus for midiNoteNumber
            addNoteOnStatus (midiNoteNumber, curTime);
            // set saw frequency and gate
             sawControl.setParamValue("/saw/freq",curFreq);
             sawControl.setParamValue("/saw/gate",1);
        }
        else{ // some statuses exist
            int activeNoteOnStatusIndex = indexOfActiveNoteOnStatus();
            int indexOfMidiNoteNumber = indexOfNoteOnStatusMatchingMidiNumber(midiNoteNumber);
            // if none are active
            if (activeNoteOnStatusIndex == -1){
                // if midiNoteNumber is already in vector
                if (indexOfMidiNoteNumber != -1){
                    // activate it
                    noteOnStatuses[indexOfMidiNoteNumber]->active = true;
                    // set saw frequency
                    sawControl.setParamValue("/saw/freq",curFreq);
                    // turn on saw gate
                    sawControl.setParamValue("/saw/gate",1);
                }
                else{ // it isn't in vector
                    // add it
                    addNoteOnStatus (midiNoteNumber, curTime);
                    // set saw frequency
                    sawControl.setParamValue("/saw/freq",curFreq);
                    // turn on saw gate
                    sawControl.setParamValue("/saw/gate",1);
                }
            }
            else{ // one is active (gate is already on)
                // if active element isn't midiNoteNumber
                if (activeNoteOnStatusIndex != indexOfMidiNoteNumber){
                    // inactivate active element
                    noteOnStatuses[activeNoteOnStatusIndex]->active = false;
                    
                    // if midiNoteNumber is already in vector
                    if (indexOfMidiNoteNumber != -1){
                        // activate it
                        noteOnStatuses[indexOfMidiNoteNumber]->active = true;
                        // set saw frequency
                        sawControl.setParamValue("/saw/freq",curFreq);
                    }
                    else{ // it isn't in vector
                        // add it
                        addNoteOnStatus (midiNoteNumber, curTime);
                        // set saw frequency
                        sawControl.setParamValue("/saw/freq",curFreq);
                    }
                }
            }
        }
    }

    void handleNoteOff (MidiKeyboardState*, int midiChannel, int midiNoteNumber, float /*velocity*/) override
    {   
        // if only 1 noteOnStatus
        if (noteOnStatuses.size() == 1){
            // delete it
            deleteNoteOnStatus(0);
            // turn off saw gate
            sawControl.setParamValue("/saw/gate",0);
        }
        else { // multiple noteOnStatuses exist
            // get index of midiNoteNumber
            int indexOfMidiNoteNumber = indexOfNoteOnStatusMatchingMidiNumber(midiNoteNumber);
            // if it is the active noteOnStatus
            if (indexOfMidiNoteNumber == indexOfActiveNoteOnStatus()){
                // delete it
                deleteNoteOnStatus(indexOfMidiNoteNumber);
                // find newest inactive noteOnStatus
                int newestInactiveNoteOnStatusIndex = newestInactiveNoteOnIndex();
                // make it active
                noteOnStatuses[newestInactiveNoteOnStatusIndex]->active = true;
                // get it's frequency in Hertz
                double curFreq = MidiMessage::getMidiNoteInHertz(noteOnStatuses[newestInactiveNoteOnStatusIndex]->midiNoteNumber);
                // change freq of saw
                sawControl.setParamValue("/saw/freq",curFreq);
            }
            else { // it isn't active
                // just delete it
                deleteNoteOnStatus(indexOfMidiNoteNumber);
            }            
        }
    }

    // function to determine if midiNoteNumber is currently in noteOnStatuses vector
    bool noteOnStatusExists(int midiNoteNumberLoc){
        int numNoteOnStatuses = noteOnStatuses.size();
        bool found = false;
        
        if (numNoteOnStatuses == 1){
            if (noteOnStatuses[0]->midiNoteNumber == midiNoteNumberLoc)
                found = true;
        } 
        else if (numNoteOnStatuses > 1){
            for (int i = 0; i < numNoteOnStatuses; i++){
                if (noteOnStatuses[i]->midiNoteNumber == midiNoteNumberLoc)
                    found = true;
            }
        }
        return found;
    }

    // function to add new miniNoteNumber to noteOnStatuses vector
    void addNoteOnStatus (int midiNoteNumberLoc, double curTime){
        if (!noteOnStatusExists(midiNoteNumberLoc)){
            noteOnStatuses.push_back(new noteOnStatus(midiNoteNumberLoc,curTime));
        }
    }

    // function to delete specific note on object
    void deleteNoteOnStatus (int index){
        std::vector<noteOnStatus *>::iterator it = noteOnStatuses.begin() + index;
        delete (*it);
        noteOnStatuses.erase(it);
    }

    // function to return index of noteOnStatus with passed in midiNoteNumber
    // -1 indicates object not found
    int indexOfNoteOnStatusMatchingMidiNumber(int midiNoteNumberLoc){
        int index = -1;
        for (int i = 0; i < noteOnStatuses.size(); i++)
        {
            if (noteOnStatuses[i]->midiNoteNumber == midiNoteNumberLoc)
                index = i;
        }
        return index;
    }

    // function to return index of inactive noteOnStatus with newest timestamp
    // -1 indicates no objects found
    int newestInactiveNoteOnIndex(){
        int index = -1;
        for (int i = 0; i < noteOnStatuses.size(); i++){
            if (noteOnStatuses[i]->active == false){
                if (index == -1)
                    index = i;
                else if (noteOnStatuses[i]->startTime > noteOnStatuses[index]->startTime)
                    index = i;
            }
        }
        return index;
    }
    
    // function to return index of active noteOnStatus (assuming at most 1)
    // -1 indicates object not found
    int indexOfActiveNoteOnStatus(){
        int index = -1;
        for (int i = 0; i < noteOnStatuses.size(); i++){
            if (noteOnStatuses[i]->active == true)
                index = i;
        }
        return index;
    }

private:
    //==============================================================================

    // FAUST and GUI stuff
   // struct to keep track of status of particular midi note number
   struct noteOnStatus {
        int midiNoteNumber;
        double startTime;
        bool active;
        noteOnStatus (int midiNoteNumberLoc, double curTime) : midiNoteNumber(midiNoteNumberLoc), 
                                                               startTime(curTime), active(true){};
        noteOnStatus () : midiNoteNumber(0), startTime(0.0), active(false){};
    };
    // vector of all current noteOn events (to keep track of pressed keys)
    std::vector<noteOnStatus *> noteOnStatuses;
    
    //SawStruct saw;
    Saw saw;
    MapUI sawControl;
    
    // Slider objects and corresponding labels
    Slider gainSlider, attackSlider, decaySlider, sustainSlider, releaseSlider, lfoFreqSlider, autowahSlider;
    Label gainLabel, attackLabel, decayLabel, sustainLabel, releaseLabel, lfoFreqLabel, titleLabel, autowahLabel;
    ToggleButton bypassLfoButton;
    
    int nChans;
    double currentSampleRate, defGainVal, defAttackVal, defDecayVal, defSustainVal, defReleaseVal,
           defLfoFreqVal;
    
    float** audioBuffer; // multichannel audio buffer used for I/O

    Font labelsFont, titleFont;
    // MIDI stuff
    AudioDeviceManager deviceManager;
    ComboBox midiInputList;
    Label midiInputListLabel;
    int lastInputIndex;                         // [3]
    bool isAddingFromMidiInput;                 // [4]
    MidiKeyboardState keyboardState;            // [5]
    MidiKeyboardComponent keyboardComponent;    // [6]

    double startTime;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};


// (This function is called by the app startup code to create our main component)
Component* createMainContentComponent()     { return new MainContentComponent(); }


#endif  // MAINCOMPONENT_H_INCLUDED

# SpaceBass - MUS256a, HW #2

This application is a standalone audio application that implements a monophonic bass synthesizer. The app consists of a single bandlimited sawtooth oscillator that can be routed through an autowah and an LFO that modulates the amplitude of the sawtooth oscillator.

---

Implemented by Michael Olsen (mjolsenATccrmaDOTstanfordDOTedu) based on starter code by Romain Michon (rmichonATccrmaDOTstanfordDOTedu) for Music 256a / CS 476a (fall 2016).
